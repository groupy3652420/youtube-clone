import { Stack } from "@mui/material";
import React from "react";
import { categories } from "../utils/constants";

const SideBar = (props) => {
  const { selectedCategory, setSelectedCategory } = props;
  return (
    <Stack
      direction="row"
      sx={{
        overflowY: "auto",
        height: { sx: "auto", md: "95%" },
        flexDirection: { md: "column" },
      }}
    >
      {categories.map((cat) => (
        <button
          key={cat.name}
          className="category-btn"
          style={{
            background: cat.name === selectedCategory && "#FC1503",
            color: "white",
          }}
          onClick={() => {
            setSelectedCategory(cat.name);
          }}
        >
          <span
            style={{
              marginRight: "15px",
              color: cat.name === selectedCategory ? "white" : "red",
              opacity: cat.name === selectedCategory ? "1" : "0.8",
            }}
          >
            {cat.icon}
          </span>
          <span>{cat.name}</span>
        </button>
      ))}
    </Stack>
  );
};

export default SideBar;
